//
//  Painter.swift
//  SampleProject
//
//  Created by Stoyko Kolev on 5.02.23.
//

import Foundation

enum Painter {

    case leonardo
    case stoyko
}
