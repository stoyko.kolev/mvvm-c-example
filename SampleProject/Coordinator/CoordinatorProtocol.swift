//
//  CoordinatorProtocol.swift
//  SampleProject
//
//  Created by Stoyko Kolev on 5.02.23.
//

import UIKit

/// Protocol defining functionality for coordinators.
protocol CoordinatorProtocol: AnyObject {

    var parent: CoordinatorProtocol? { get set }

    /// Collection of child coordinators.
    var children: [CoordinatorProtocol] { get set }

    /// Navigation controller to control the flow.
    var navigationController: UINavigationController { get }

    /// Starts the navigation flow.
    func start()

    /// Releases the coordinator and dependancies when navigation flow is finished.
    func finish()

    /// Removes a child coordinator from its parent, releasing it from memory.
    /// - Parameter childCoordinator: The coordinator to be removed from children collection.
    func removeChildCoordinator(_ childCoordinator: CoordinatorProtocol)

    /// Adds a child coordinator to the parent, preventing it from getting deallocated in memory.
    /// - Parameter childCoordinator: The coordinator to be added to children collection.
    func addChildCoordinator(_ childCoordinator: CoordinatorProtocol)

}
