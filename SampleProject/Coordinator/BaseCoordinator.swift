//
//  BaseCoordinator.swift
//  SampleProject
//
//  Created by Stoyko Kolev on 5.02.23.
//

import UIKit

class BaseCoordinator: CoordinatorProtocol {

    // MARK: - Properties

    weak var parent: CoordinatorProtocol?
    var children: [CoordinatorProtocol] = []
    var navigationController: UINavigationController

    // MARK: - Initializers

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    // MARK: - Public Functions
    
    func start() {
    }

    func finish() {
    }

    func removeChildCoordinator(_ childCoordinator: CoordinatorProtocol) {
        children = children.filter { $0 !== childCoordinator }
    }

    func addChildCoordinator(_ childCoordinator: CoordinatorProtocol) {
        childCoordinator.parent = self
        children.append(childCoordinator)
        childCoordinator.start()
    }
}

// MARK: - Setup Navigation

extension BaseCoordinator {

    /// Sets up navigation controller appearance
    /// - Parameters:
    ///   - image: Tab bar item image to be visualized
    ///   - rootViewController: View controller navigation flow should start with.
    func setupNavigationController(image: UIImage?, rootViewController: UIViewController) {
        navigationController.tabBarItem.image = image
        navigationController.viewControllers = [rootViewController]
    }
}
