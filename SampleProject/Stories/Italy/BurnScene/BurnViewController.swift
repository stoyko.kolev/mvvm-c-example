//
//  BurnViewController.swift
//  SampleProject
//
//  Created by Stoyko Kolev on 5.02.23.
//

import UIKit

protocol BurnViewModelProtocol {

    var paintng: Painting { get }

    func leave()
}

class BurnViewController: UIViewController {

    // MARK: - Properties

    private var viewModel: BurnViewModelProtocol!

    // MARK: - IBOutlets

    @IBOutlet private weak var imageView: UIImageView!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = UIImage(named: viewModel.paintng.name)
    }

    // MARK: - IBActions

    @IBAction private func didTapLeaveButton(_ sender: UIButton) {
        viewModel.leave()
    }
}

// MARK: - Instantiate

extension BurnViewController {

    static func create(painting: Painting, delegate: ItalyCoordinationDelegate) -> UIViewController {
        let viewController = BurnViewController()
        let viewModel = BurnViewModel(paintng: painting)
        viewModel.delegate = delegate
        viewController.viewModel = viewModel
        return viewController
    }
}
