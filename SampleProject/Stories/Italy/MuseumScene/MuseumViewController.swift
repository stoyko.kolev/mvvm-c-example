//
//  MuseumViewController.swift
//  SampleProject
//
//  Created by Stoyko Kolev on 6.02.23.
//

import UIKit

protocol MuseumViewModelProtocol {

    var paintng: Painting { get }

    func leave()
}

class MuseumViewController: UIViewController {

    // MARK: - Properties

    private var viewModel: MuseumViewModelProtocol!

    // MARK: - IBOutlets

    @IBOutlet private weak var imageView: UIImageView!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = UIImage(named: viewModel.paintng.name)
    }

    // MARK: - IBActions

    @IBAction private func didTapLeaveButton(_ sender: UIButton) {
        viewModel.leave()
    }
}

// MARK: - Instantiate

extension MuseumViewController {

    static func create(painting: Painting, delegate: ItalyCoordinationDelegate) -> UIViewController {
        let viewController = MuseumViewController()
        let viewModel = MuseumViewModel(paintng: painting)
        viewModel.delegate = delegate
        viewController.viewModel = viewModel
        return viewController
    }
}

