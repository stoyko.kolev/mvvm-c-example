//
//  MuseumViewModel.swift
//  SampleProject
//
//  Created by Stoyko Kolev on 6.02.23.
//

import Foundation

class MuseumViewModel: MuseumViewModelProtocol {

    // MARK: - Properties

    let paintng: Painting
    weak var delegate: ItalyCoordinationDelegate?

    // MARK: - Initializer

    init(paintng: Painting) {
        self.paintng = paintng
    }

    // MARK: - Public Functions

    func leave() {
        delegate?.didFinishScene(action: .leaveMuseum)
    }
}
