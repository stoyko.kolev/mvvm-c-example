//
//  SelectPainterViewModel.swift
//  SampleProject
//
//  Created by Stoyko Kolev on 5.02.23.
//

import Foundation

class SelectPainterViewModel: SelectPainterViewModelProtocol {

    // MARK: - Propertis

    weak var delegate: ItalyCoordinationDelegate?
    
    // MARK: - Public Functions

    func selectPainter(_ painter: Painter) {
        delegate?.didFinishScene(action: .select(painter: painter))
    }
}
