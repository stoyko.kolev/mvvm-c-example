//
//  SelectPainterViewController.swift
//  SampleProject
//
//  Created by Stoyko Kolev on 5.02.23.
//

import UIKit

protocol SelectPainterViewModelProtocol {

    func selectPainter(_ painter: Painter)
}

class SelectPainterViewController: UIViewController {

    // MARK: - Properties

    private var viewModel: SelectPainterViewModelProtocol!

    // MARK: - IBActions

    @IBAction private func didTapLeonardoButton(_ sender: UIButton) {
        viewModel.selectPainter(.leonardo)
    }

    @IBAction private func didTapStoykoButton(_ sender: UIButton) {
        viewModel.selectPainter(.stoyko)
    }
}

// MARK: - Instantiate

extension SelectPainterViewController {

    static func create(delegate: ItalyCoordinationDelegate) -> UIViewController {
        let viewController = SelectPainterViewController()
        let viewModel = SelectPainterViewModel()
        viewModel.delegate = delegate
        viewController.viewModel = viewModel
        return viewController
    }
}
