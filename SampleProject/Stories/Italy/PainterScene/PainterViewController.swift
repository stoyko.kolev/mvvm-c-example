//
//  PainterViewController.swift
//  SampleProject
//
//  Created by Stoyko Kolev on 5.02.23.
//

import UIKit

protocol PainterViewModelProtocol {

    var painting: Observable<Painting?> { get }

    func paint()
    func sell()
    func burn()
}

class PainterViewController: UIViewController {

    // MARK: - Properties

    private var viewModel: PainterViewModelProtocol!

    // MARK: - IBOutlets

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var paintButton: UIButton!
    @IBOutlet private weak var sellButton: UIButton!
    @IBOutlet private weak var burnButton: UIButton!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBinding()
    }

    // MARK: - IBActions

    @IBAction private func didTapPaintButton(_ sender: UIButton) {
        viewModel.paint()
    }
    @IBAction private func didTapSellButton(_ sender: UIButton) {
        viewModel.sell()
    }
    @IBAction private func didTapBurnButton(_ sender: UIButton) {
        viewModel.burn()
    }
}

// MARK: - Helpers

extension PainterViewController {

    private func setupBinding() {
        viewModel.painting.sink { [weak self] painting in
            guard let painting = painting else { return }

            self?.updateUI(painting: painting)
        }
    }

    private func updateUI(painting: Painting) {
        paintButton.isHidden = true
        sellButton.isHidden = false
        burnButton.isHidden = false
        imageView.image = UIImage(named: painting.name)
        imageView.layer.borderWidth = 5
    }
}

// MARK: - Instantiate

extension PainterViewController {

    static func create(painter: Painter, delegate: ItalyCoordinationDelegate) -> UIViewController {
        let viewController = PainterViewController()
        switch painter {
        case .leonardo:
            let viewModel = LeonardoViewModel()
            viewModel.delegate = delegate
            viewController.viewModel = viewModel
        case .stoyko:
            let viewModel = StoykoViewModel()
            viewModel.delegate = delegate
            viewController.viewModel = viewModel
        }
        return viewController
    }
}
