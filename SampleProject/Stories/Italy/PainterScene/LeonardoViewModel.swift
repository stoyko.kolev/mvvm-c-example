//
//  LeonardoViewModel.swift
//  SampleProject
//
//  Created by Stoyko Kolev on 5.02.23.
//

import Foundation

class LeonardoViewModel: PainterViewModelProtocol {

    // MARK: - Properties

    var painting: Observable<Painting?>
    weak var delegate: ItalyCoordinationDelegate?

    // MARK: - Initializers

    init(painting: Observable<Painting?> = Observable(nil)) {
        self.painting = painting
    }

    // MARK: - Public Functions

    func paint() {
        painting.value = Painting(name: "monalisa")
    }

    func sell() {
        guard let painting = painting.value else { return }

        delegate?.didFinishScene(action: .sell(painting: painting))
    }

    func burn() {
        guard let painting = painting.value else { return }

        delegate?.didFinishScene(action: .burn(painting: painting))
    }
}
