//
//  PopeCoordinator.swift
//  SampleProject
//
//  Created by Stoyko Kolev on 5.02.23.
//

import UIKit

enum ItalyCoordinationAction {
    case select(painter: Painter)
    case sell(painting: Painting)
    case burn(painting: Painting)
    case leavePyre
    case leaveMuseum
}

protocol ItalyCoordinationDelegate: AnyObject {

    func didFinishScene(action: ItalyCoordinationAction)
}

class PopeCoordinator: BaseCoordinator {

    // MARK: - Properties

    private var window: UIWindow

    // MARK: - Initializers

    init(window: UIWindow,
         navigationController: UINavigationController = UINavigationController()) {
        self.window = window
        super.init(navigationController: navigationController)
    }

    // MARK: - Public Functions

    override func start() {
        setupWindow()
        showSelectPainterScene()
    }
}

// MARK: - Helpers

extension PopeCoordinator {

    private func setupWindow() {
        window.makeKeyAndVisible()
        window.rootViewController = navigationController
    }

    private func showSelectPainterScene() {
        let viewController = SelectPainterViewController.create(delegate: self)
        navigationController.viewControllers = [viewController]
    }

    private func showPainterScene(with painter: Painter) {
        let viewController = PainterViewController.create(painter: painter, delegate: self)
        navigationController.pushViewController(viewController, animated: true)
    }

    private func showMuseumScene(with painting: Painting) {
        let viewController = MuseumViewController.create(painting: painting, delegate: self)
        navigationController.pushViewController(viewController, animated: true)
    }

    private func showBurnScene(with painting: Painting) {
        let viewController = BurnViewController.create(painting: painting, delegate: self)
        navigationController.pushViewController(viewController, animated: true)
    }
}

// MARK: - ItalyCoordinationDelegate

extension PopeCoordinator: ItalyCoordinationDelegate {

    func didFinishScene(action: ItalyCoordinationAction) {
        switch action {
        case .select(let painter):
            showPainterScene(with: painter)
        case .sell(let painting):
            showMuseumScene(with: painting)
        case .burn(let painting):
            showBurnScene(with: painting)
        case .leavePyre, .leaveMuseum:
            navigationController.popToRootViewController(animated: true)
        }
    }
}
